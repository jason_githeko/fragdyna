package com.example.fragdyna;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
    }

    public void selectFrag(View view) {

        Fragment fr;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        if (view == findViewById(R.id.button2)) { //Button 2 pressed
            fr = new FragmentTwo(); //Instantiate Fragement One
            fragmentTransaction.replace(R.id.fragment_place, fr);
            fragmentTransaction.commit();


        } else if (view == findViewById(R.id.button1)) { //Button 1 pressed){                                  //Button 1 pressed
            fr = new FragmentOne(); //Instantiate Fragment One
            fragmentTransaction.replace(R.id.fragment_place, fr);
            fragmentTransaction.commit();

        }
// Use FragmentManager to create FragmentTransaction


    }
}
